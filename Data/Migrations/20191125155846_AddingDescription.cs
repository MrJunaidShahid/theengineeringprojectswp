﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TheEngineeringProjectsWP.Data.Migrations
{
    public partial class AddingDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Post",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Post");
        }
    }
}
