﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheEngineeringProjectsWP.Data.Migrations
{
    public partial class GeneralSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GeneralSettings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SiteTitle = table.Column<string>(nullable: true),
                    Tagline = table.Column<string>(nullable: true),
                    WordPressAddressURL = table.Column<string>(nullable: true),
                    SiteAddressURL = table.Column<string>(nullable: true),
                    AdminEmail = table.Column<string>(nullable: true),
                    AnyoneCanRegister = table.Column<bool>(nullable: false),
                    DefaultRole = table.Column<string>(nullable: true),
                    SiteLanguage = table.Column<string>(nullable: true),
                    Timezone = table.Column<string>(nullable: true),
                    DateFormat = table.Column<string>(nullable: true),
                    TimeFormat = table.Column<string>(nullable: true),
                    WeekStartDay = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralSettings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GeneralSettings");
        }
    }
}
