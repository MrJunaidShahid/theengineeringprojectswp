﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TheEngineeringProjectsWP.Data.Migrations
{
    public partial class WordPressDatabaseMigratedModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

           
           
            migrationBuilder.CreateTable(
                name: "WpCommentmeta",
                columns: table => new
                {
                    MetaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentId = table.Column<int>(nullable: false),
                    MetaKey = table.Column<string>(nullable: true),
                    MetaValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpCommentmeta", x => x.MetaId);
                });

            migrationBuilder.CreateTable(
                name: "WpComments",
                columns: table => new
                {
                    CommentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommentPostId = table.Column<int>(nullable: false),
                    CommentAuthor = table.Column<string>(nullable: true),
                    CommentAuthorEmail = table.Column<string>(nullable: true),
                    CommentAuthorUrl = table.Column<string>(nullable: true),
                    CommentAuthorIp = table.Column<string>(nullable: true),
                    CommentDate = table.Column<DateTime>(nullable: false),
                    CommentDateGmt = table.Column<DateTime>(nullable: false),
                    CommentContent = table.Column<string>(nullable: true),
                    CommentKarma = table.Column<int>(nullable: false),
                    CommentApproved = table.Column<string>(nullable: true),
                    CommentAgent = table.Column<string>(nullable: true),
                    CommentType = table.Column<string>(nullable: true),
                    CommentParent = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpComments", x => x.CommentId);
                });

            migrationBuilder.CreateTable(
                name: "WpLinks",
                columns: table => new
                {
                    LinkId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LinkUrl = table.Column<string>(nullable: true),
                    LinkName = table.Column<string>(nullable: true),
                    LinkImage = table.Column<string>(nullable: true),
                    LinkTarget = table.Column<string>(nullable: true),
                    LinkDescription = table.Column<string>(nullable: true),
                    LinkVisible = table.Column<string>(nullable: true),
                    LinkOwner = table.Column<int>(nullable: false),
                    LinkRating = table.Column<int>(nullable: false),
                    LinkUpdated = table.Column<DateTime>(nullable: false),
                    LinkRel = table.Column<string>(nullable: true),
                    LinkNotes = table.Column<string>(nullable: true),
                    LinkRss = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpLinks", x => x.LinkId);
                });

            migrationBuilder.CreateTable(
                name: "WpOptions",
                columns: table => new
                {
                    OptionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    OptionName = table.Column<string>(nullable: true),
                    OptionValue = table.Column<string>(nullable: true),
                    Autoload = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpOptions", x => x.OptionId);
                });

            migrationBuilder.CreateTable(
                name: "WpPostmeta",
                columns: table => new
                {
                    MetaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PostId = table.Column<int>(nullable: false),
                    MetaKey = table.Column<string>(nullable: true),
                    MetaValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpPostmeta", x => x.MetaId);
                });

            migrationBuilder.CreateTable(
                name: "WpPosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PostAuthor = table.Column<int>(nullable: false),
                    PostDate = table.Column<DateTime>(nullable: false),
                    PostDateGmt = table.Column<DateTime>(nullable: false),
                    PostContent = table.Column<string>(nullable: true),
                    PostTitle = table.Column<string>(nullable: true),
                    PostExcerpt = table.Column<string>(nullable: true),
                    PostStatus = table.Column<string>(nullable: true),
                    CommentStatus = table.Column<string>(nullable: true),
                    PingStatus = table.Column<string>(nullable: true),
                    PostPassword = table.Column<string>(nullable: true),
                    PostName = table.Column<string>(nullable: true),
                    ToPing = table.Column<string>(nullable: true),
                    Pinged = table.Column<string>(nullable: true),
                    PostModified = table.Column<DateTime>(nullable: false),
                    PostModifiedGmt = table.Column<DateTime>(nullable: false),
                    PostContentFiltered = table.Column<string>(nullable: true),
                    PostParent = table.Column<decimal>(nullable: false),
                    Guid = table.Column<string>(nullable: true),
                    MenuOrder = table.Column<int>(nullable: false),
                    PostType = table.Column<string>(nullable: true),
                    PostMimeType = table.Column<string>(nullable: true),
                    CommentCount = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpPosts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WpTermmeta",
                columns: table => new
                {
                    MetaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TermId = table.Column<int>(nullable: false),
                    MetaKey = table.Column<string>(nullable: true),
                    MetaValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpTermmeta", x => x.MetaId);
                });

            migrationBuilder.CreateTable(
                name: "WpTermRelationships",
                columns: table => new
                {
                    ObjectId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TermTaxonomyId = table.Column<int>(nullable: false),
                    TermOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpTermRelationships", x => x.ObjectId);
                });

            migrationBuilder.CreateTable(
                name: "WpTerms",
                columns: table => new
                {
                    TermId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true),
                    TermGroup = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpTerms", x => x.TermId);
                });

            migrationBuilder.CreateTable(
                name: "WpTermTaxonomy",
                columns: table => new
                {
                    TermTaxonomyId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TermId = table.Column<int>(nullable: false),
                    Taxonomy = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Parent = table.Column<int>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpTermTaxonomy", x => x.TermTaxonomyId);
                });

            migrationBuilder.CreateTable(
                name: "WpUsermeta",
                columns: table => new
                {
                    UmetaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    MetaKey = table.Column<string>(nullable: true),
                    MetaValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpUsermeta", x => x.UmetaId);
                });

            migrationBuilder.CreateTable(
                name: "WpUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserLogin = table.Column<string>(nullable: true),
                    UserPass = table.Column<string>(nullable: true),
                    UserNicename = table.Column<string>(nullable: true),
                    UserEmail = table.Column<string>(nullable: true),
                    UserUrl = table.Column<string>(nullable: true),
                    UserRegistered = table.Column<DateTime>(nullable: false),
                    UserActivationKey = table.Column<string>(nullable: true),
                    UserStatus = table.Column<int>(nullable: false),
                    DisplayName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WpUsers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WpCommentmeta");

            migrationBuilder.DropTable(
                name: "WpComments");

            migrationBuilder.DropTable(
                name: "WpLinks");

            migrationBuilder.DropTable(
                name: "WpOptions");

            migrationBuilder.DropTable(
                name: "WpPostmeta");

            migrationBuilder.DropTable(
                name: "WpPosts");

            migrationBuilder.DropTable(
                name: "WpTermmeta");

            migrationBuilder.DropTable(
                name: "WpTermRelationships");

            migrationBuilder.DropTable(
                name: "WpTerms");

            migrationBuilder.DropTable(
                name: "WpTermTaxonomy");

            migrationBuilder.DropTable(
                name: "WpUsermeta");

            migrationBuilder.DropTable(
                name: "WpUsers");

        }
    }
}
