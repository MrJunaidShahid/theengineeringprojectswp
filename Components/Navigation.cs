﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Components
{
    public class Navigation : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(string EmptyString)
        {
            /* == Place Logic Here == */
            return View();
        }
    }
}
