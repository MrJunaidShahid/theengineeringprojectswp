﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models
{
    public class GeneralSettings
    {
        [Key]
        public int Id { get; set; }
        public string SiteTitle { get; set; }
        public string Tagline { get; set; }
        public string WordPressAddressURL { get; set; }
        public string SiteAddressURL { get; set; }
        public string AdminEmail { get; set; }
        public bool AnyoneCanRegister { get; set; }
        public string DefaultRole { get; set; }
        public string SiteLanguage { get; set; }
        public string Timezone { get; set; }
        public string DateFormat { get; set; }
        public string TimeFormat { get; set; }
        public string WeekStartDay { get; set; }
    }
}
