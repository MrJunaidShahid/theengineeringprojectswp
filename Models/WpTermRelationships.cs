﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheEngineeringProjectsWP.Models
{
    public partial class WpTermRelationships
    {
        [Key]
        public int ObjectId { get; set; }
        public int TermTaxonomyId { get; set; }
        public int TermOrder { get; set; }
    }
}
