﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheEngineeringProjectsWP.Models
{
    public partial class WpTerms
    {
        [Key]
        public int TermId { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public int TermGroup { get; set; }
    }
}
