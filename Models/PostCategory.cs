﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models
{
    public class PostCategory
    {
        [Key]
        public int CategoryId { get; set; }
        public int ParentCategoryId { get; set; }
        [Required]
        public string CategoryTitle { get; set; }
        [Required]
        public string CategoryDescription { get; set; }
    }
}
