﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models
{
    public class PostTag
    {
        [Key]
        public int TagId { get; set; }
        public string TagTitle { get; set; }
        public string TagDescription { get; set; }
    }
}
