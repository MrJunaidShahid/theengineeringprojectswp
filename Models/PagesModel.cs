﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TheEngineeringProjectsWP.Models
{
    public class PagesModel
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }    
        public string FeatureImage { get; set; }
        public bool IsPublic { get; set; }
        public bool IsAllowComments { get; set; }
    }
}
