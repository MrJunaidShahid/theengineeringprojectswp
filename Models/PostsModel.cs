﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TheEngineeringProjectsWP.Models
{
    public class PostsModel
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string Description { get; set; }
        public string FeatureImage { get; set; }
        public bool IsPublic { get; set; }
        public bool IsAllowComments { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CategoryId { get; set; }
        public int TagId { get; set; }
    }
}
