﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheEngineeringProjectsWP.Models
{
    public partial class WpOptions
    {
        [Key]
        public int OptionId { get; set; }
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
        public string Autoload { get; set; }
    }
}
