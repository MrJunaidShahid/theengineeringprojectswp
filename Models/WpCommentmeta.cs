﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheEngineeringProjectsWP.Models
{
    public partial class WpCommentmeta
    {
        [Key]
        public int MetaId { get; set; }
        public int CommentId { get; set; }
        public string MetaKey { get; set; }
        public string MetaValue { get; set; }
    }
}
