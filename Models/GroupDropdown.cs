﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models
{
    public class GroupDropdown
    {
        public string optgroup { get; set; }
        public List<GroupDropdownList> options { get; set; }
    }

    public class GroupDropdownList
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
