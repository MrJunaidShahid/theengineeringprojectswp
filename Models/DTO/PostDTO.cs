﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models.DTO
{
    public class PostDTO : PostsModel
    {
        public IFormFile Image { get; set; }
    }
}
