﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheEngineeringProjectsWP.Models.DTO
{
    public class BlogDTO : PostsModel
    {
        public string CategoryName { get; set; }
        public string TagName { get; set; }
        public int Comments { get; set; }
        public int Views { get; set; }
    }
}
