﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TheEngineeringProjectsWP.Data;
using TheEngineeringProjectsWP.Models;
using TheEngineeringProjectsWP.Models.DTO;

namespace TheEngineeringProjectsWP.Controllers
{
    public class PostsModelsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PostsModelsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PostsModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.Post.ToListAsync());
        }

        // GET: PostsModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postsModel = await _context.Post
                .FirstOrDefaultAsync(m => m.Id == id);
            if (postsModel == null)
            {
                return NotFound();
            }

            return View(postsModel);
        }

        // GET: PostsModels/Create
        public IActionResult Create()
        {
            ViewBag.Category = _context.Categories.Select(x => new SelectListItem { Value = x.CategoryId.ToString(), Text = x.CategoryTitle }).ToList();
            ViewBag.Tag = _context.Tags.Select(x=> new SelectListItem { Value = x.TagId.ToString(), Text = x.TagTitle}).ToList();
            return View();
        }

        // POST: PostsModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PostDTO postsModel)
        {
            if (ModelState.IsValid)
            {
                PostsModel Data = new PostsModel();
                Data.Title = postsModel.Title;
                Data.Content = postsModel.Content;
                Data.Description = postsModel.Description;
                Data.IsPublic = postsModel.IsPublic;
                Data.IsAllowComments = postsModel.IsAllowComments;
                Data.CategoryId = postsModel.CategoryId;
                Data.TagId = postsModel.TagId;
                Data.CreatedOn = DateTime.Now;
                if (postsModel.Image == null)
                {
                    Data.FeatureImage = "image-preview.png";
                }
                else
                {
                    string FileName = Path.GetFileName(postsModel.Image.FileName);
                    string FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", FileName);
                    using (var stream = new FileStream(FilePath, FileMode.Create))
                    {
                        await postsModel.Image.CopyToAsync(stream);
                    }
                    Data.FeatureImage = FileName;
                }

                _context.Add(Data);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(postsModel);
        }

        // GET: PostsModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            ViewBag.Category = _context.Categories.Select(x => new SelectListItem { Value = x.CategoryId.ToString(), Text = x.CategoryTitle }).ToList();
            ViewBag.Tag = _context.Tags.Select(x => new SelectListItem { Value = x.TagId.ToString(), Text = x.TagTitle }).ToList();

            var postsModel = await _context.Post.FindAsync(id);
            if (postsModel == null)
            {
                return NotFound();
            }
            return View(postsModel);
        }

        // POST: PostsModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PostDTO postsModel)
        {
            if (id != postsModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    PostsModel Data = new PostsModel();
                    Data = _context.Post.Where(x => x.Id == id).FirstOrDefault();
                    Data.Title = postsModel.Title;
                    Data.Content = postsModel.Content;
                    Data.Description = postsModel.Description;
                    Data.IsAllowComments = postsModel.IsAllowComments;
                    Data.IsPublic = postsModel.IsPublic;
                    Data.CategoryId = postsModel.CategoryId;
                    Data.TagId = postsModel.TagId;
                    if (postsModel.Image == null)
                    {
                        // Data.FeatureImage = "image-preview.png";

                    }
                    else
                    {
                        string FileName = Path.GetFileName(postsModel.Image.FileName);
                        string FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", FileName);
                        using (var stream = new FileStream(FilePath, FileMode.Create))
                        {
                            await postsModel.Image.CopyToAsync(stream);
                        }
                        Data.FeatureImage = FileName;
                    }

                    _context.Update(Data);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostsModelExists(postsModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(postsModel);
        }



        // GET: PostsModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postsModel = await _context.Post
                .FirstOrDefaultAsync(m => m.Id == id);
            if (postsModel == null)
            {
                return NotFound();
            }

            return View(postsModel);
        }

        // POST: PostsModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var postsModel = await _context.Post.FindAsync(id);
            _context.Post.Remove(postsModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostsModelExists(int id)
        {
            return _context.Post.Any(e => e.Id == id);
        }
    }
}
