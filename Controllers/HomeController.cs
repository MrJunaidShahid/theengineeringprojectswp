﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using TheEngineeringProjectsWP.Data;
using TheEngineeringProjectsWP.Models;
using TheEngineeringProjectsWP.Models.DTO;

namespace TheEngineeringProjectsWP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            List <BlogDTO> data = new List<BlogDTO>();
            var LatestPost = _context.Post.Where(x => x.IsPublic == true).Take(10).OrderByDescending(x => x.Id).ToList();

            foreach (var item in LatestPost)
            {
                PostTag Tag = _context.Tags.Where(x => x.TagId == item.TagId).FirstOrDefault();
                string TempTagName = "";
                if(Tag == null)
                {
                    TempTagName = "";
                }
                else
                {
                    TempTagName = Tag.TagTitle;
                }

                PostCategory Category = _context.Categories.Where(x => x.CategoryId == item.CategoryId).FirstOrDefault();
                string TempCategoryName = "";
                if (Category == null)
                {
                    TempCategoryName = "";
                }
                else
                {
                    TempCategoryName = Category.CategoryTitle;
                }

                BlogDTO Temp = new BlogDTO();
                Temp.CategoryId = item.CategoryId;
                Temp.CategoryName = TempCategoryName;
                Temp.Comments = 0;
                Temp.Content = item.Content;
                Temp.Description = item.Description;
                Temp.CreatedOn = item.CreatedOn;
                Temp.FeatureImage = item.FeatureImage;
                Temp.Id = item.Id;
                Temp.TagId = item.TagId;
                Temp.TagName = TempTagName;
                Temp.Title = item.Title;
                Temp.Views = 0;

                data.Add(Temp);
            }
            return View(data);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
