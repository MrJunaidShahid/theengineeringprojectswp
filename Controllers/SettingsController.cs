﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TheEngineeringProjectsWP.Data;
using TheEngineeringProjectsWP.Models;

namespace TheEngineeringProjectsWP.Controllers
{
    public class SettingsController : Controller
    {
        ApplicationDbContext _context;

        public SettingsController(ApplicationDbContext _context)
        {
            this._context = _context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult General()
        {
            GeneralSettings Result = _context.GeneralSettings.FirstOrDefault();
            if(Result == null)
            {
                Result = new GeneralSettings();
            }
            return View(Result);
        }

        [HttpPost]
        public IActionResult General(GeneralSettings _settings)
        {
           var Result =  _context.GeneralSettings.FirstOrDefault();

            if (Result == null)
            {
                _context.GeneralSettings.Add(_settings);
            }
            else
            {
                Result.AdminEmail = _settings.AdminEmail;
                Result.AnyoneCanRegister = _settings.AnyoneCanRegister;
                Result.DateFormat = _settings.DateFormat;
                Result.DefaultRole = _settings.DefaultRole;
                Result.Timezone = _settings.Timezone;
                Result.SiteAddressURL = _settings.SiteAddressURL;
                Result.SiteLanguage = _settings.SiteLanguage;
                Result.SiteTitle = _settings.SiteTitle;
                Result.Tagline = _settings.Tagline;
                Result.TimeFormat = _settings.TimeFormat;
                Result.WeekStartDay = _settings.WeekStartDay;
                Result.WordPressAddressURL = _settings.WordPressAddressURL;

                _context.GeneralSettings.Update(Result);
            }
            _context.SaveChanges();
            //_context.GeneralSettings.Add()
            return View();
        }
    }
}