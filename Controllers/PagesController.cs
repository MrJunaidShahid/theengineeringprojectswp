﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TheEngineeringProjectsWP.Data;
using TheEngineeringProjectsWP.Models;
using TheEngineeringProjectsWP.Models.DTO;

namespace TheEngineeringProjectsWP.Controllers
{
    public class PagesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Pages
        public async Task<IActionResult> Index()
        {
            return View(await _context.Page.ToListAsync());
        }

        // GET: Pages/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pagesModel = await _context.Page
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pagesModel == null)
            {
                return NotFound();
            }

            return View(pagesModel);
        }

        // GET: Pages/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PageDTO pagesModel)
        {
            if (ModelState.IsValid)
            {
                PagesModel Data = new PagesModel();
                Data.Title = pagesModel.Title;
                Data.Content = pagesModel.Content;
                Data.IsPublic = pagesModel.IsPublic;
                Data.IsAllowComments = pagesModel.IsAllowComments;

                if (pagesModel.Image == null)
                {
                    Data.FeatureImage = "image-preview.png";
                }
                else
                {
                    string FileName = Path.GetFileName(pagesModel.Image.FileName);
                    string FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", FileName);
                    using (var stream = new FileStream(FilePath, FileMode.Create))
                    {
                        await pagesModel.Image.CopyToAsync(stream);
                    }
                    Data.FeatureImage = FileName;
                }
                
                _context.Add(Data);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(pagesModel);
        }

        // GET: Pages/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pagesModel = await _context.Page.FindAsync(id);
            if (pagesModel == null)
            {
                return NotFound();
            }
            return View(pagesModel);
        }

        // POST: Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, PageDTO pagesModel)
        {
            if (id != pagesModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    PagesModel Data = new PagesModel();
                    Data = _context.Page.Where(x=>x.Id == id).FirstOrDefault();
                    Data.Title = pagesModel.Title;
                    Data.Content = pagesModel.Content;
                    Data.IsAllowComments = pagesModel.IsAllowComments;
                    Data.IsPublic = pagesModel.IsPublic;
                    if (pagesModel.Image == null)
                    {
                       // Data.FeatureImage = "image-preview.png";

                    }
                    else
                    {
                        string FileName = Path.GetFileName(pagesModel.Image.FileName);
                        string FilePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", FileName);
                        using (var stream = new FileStream(FilePath, FileMode.Create))
                        {
                            await pagesModel.Image.CopyToAsync(stream);
                        }
                        Data.FeatureImage = FileName;
                    }

                    _context.Update(Data);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PagesModelExists(pagesModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(pagesModel);
        }

        // GET: Pages/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var pagesModel = await _context.Page
                .FirstOrDefaultAsync(m => m.Id == id);
            if (pagesModel == null)
            {
                return NotFound();
            }

            return View(pagesModel);
        }

        // POST: Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var pagesModel = await _context.Page.FindAsync(id);
            _context.Page.Remove(pagesModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PagesModelExists(int id)
        {
            return _context.Page.Any(e => e.Id == id);
        }
    }
}
